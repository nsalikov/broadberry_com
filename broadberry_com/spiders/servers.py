# -*- coding: utf-8 -*-
import re
import copy
import scrapy


class ServersSpider(scrapy.Spider):
    name = 'servers'
    allowed_domains = ['broadberry.com']
    start_urls = ['https://www.broadberry.com/rackmount-servers']


    def parse(self, response):
        urls_css = '.tab-content .tab-pane a::attr(href)'
        urls = response.css(urls_css).extract()

        for url in urls:
            yield response.follow(url=url, callback=self.parse_servers)


    def parse_servers(self, response):
        for div in response.css('.tp-hold .tp'):
            item_id = div.css('div.tp::attr(data-sys-id)').extract_first()
            url = div.css('div.tp a.tp-link::attr(href)').extract_first()

            yield response.follow(url=url, callback=self.parse_server, meta={'item_id': item_id})


    def parse_server(self, response):
        d = {}

        d['type'] = 'server'
        d['item_id'] = response.meta['item_id']
        d['url'] = response.url
        d['breadcrumbs'] = response.css('.breadcrumb li a::text').extract()
        d['name'] = _clear_and_join(response.css('h1 ::text').extract(), sep=' ')

        # url = 'https://www.broadberry.com/inc/ajax/specifications/system_specifications-bootstrap.php?system_id={}'.format(d['item_id'])
        # return response.follow(url=url, callback=self.parse_server_specs, meta={'item': d})

        url = 'https://www.broadberry.com/inc/ajax/configurator/show_configurator-bootstrap.php?system_id={}'.format(d['item_id'])
        return response.follow(url=url, callback=self.parse_server_parts, meta={'item': d})


    def parse_server_specs(self, response):
        d = copy.deepcopy(response.meta['item'])
        d['specs'] = _parse_server_specs(response)

        url = 'https://www.broadberry.com/inc/ajax/configurator/show_configurator-bootstrap.php?system_id={}'.format(d['item_id'])
        return response.follow(url=url, callback=self.parse_server_parts, meta={'item': d})


    def parse_server_parts(self, response):
        d = copy.deepcopy(response.meta['item'])
        d['parts'] = _parse_server_parts(response)

        yield d

        for section in d['parts'].keys():
            for subsection in d['parts'][section].keys():
                for part in d['parts'][section][subsection]:
                    if part['item_id']:
                        url = 'https://www.broadberry.com/inc/ajax/configurator/item_info.php?item_id={}'.format(part['item_id'])
                        # url = 'https://www.broadberry.com/inc/ajax/configurator/item_info.php?system_id={}&item_id={}'.format(d['item_id'], part['item_id'])
                        yield response.follow(url=url, meta={'item_id': part['item_id']}, callback=self.parse_part_specs)


    def parse_part_specs(self, response):
        d = {}

        d['type'] = 'part'
        d['item_id'] = response.meta['item_id']
        d['url'] = response.url
        d['name'] = _clear_and_join(response.css('h3 ::text').extract(), sep=' ')
        d['description'] = _clear_and_join(response.css('.row>div>p ::text').extract())
        d['specs'] = _parse_part_specs(response)

        return d


def _clear_and_join(lst, sep='\n'):
    lst = [re.sub('[\xa0\r\t®Â ]+', ' ', t, flags=re.DOTALL).strip() for t in lst]
    lst = list(filter(None, [re.sub('\s*\n\s*', sep, t, flags=re.DOTALL).strip() for t in lst]))

    text = sep.join(lst)

    return text


def _parse_server_specs(response):
    d = {}
    d['summary'] = {}

    for tr in response.css('table.spec_tab_overview tr'):
        key = _clear_and_join(tr.css('tr > td:nth-child(1)::text').extract(), sep=' ')
        value = _clear_and_join(tr.css('tr > td:nth-child(2)::text').extract())

        if key and value:
            d[key] = value

    return d


def _parse_server_parts(response):
    parts = {}

    for holder in response.css('body>.item_type_holder'):
        section = _clear_and_join(holder.css('h4 ::text').extract())

        if not section:
            continue
        else:
            if section not in parts:
                parts[section] = {}

        subsection = 'NA'
        for item in holder.xpath('.//*[child::div[@class="item"]]/*'):
            if item.css('p.new_attribute_value'):
                subsection = _clear_and_join(item.css('p.new_attribute_value ::text').extract(), sep=' ')
            elif item.css('div.item'):
                if item.css('div.item input'):
                    item_id = item.css('div.item input ::attr(onclick)').extract_first()
                    item_id = re.sub('.*\(\s*\d+\s*,\s*(\d+)\s*.*', '\g<1>', item_id) if item_id else None
                    qty = []
                elif item.css('div.item select'):
                    item_id = item.css('div.item select ::attr(onclick)').extract_first()
                    item_id = re.sub('.*\(\s*\d+\s*,\s*(\d+).*', '\g<1>', item_id) if item_id else None
                    qty = [o.strip() for o in item.css('div.item select option ::text').extract()]
                else:
                    continue

                name = _clear_and_join(item.css('div.item label ::text').extract(), sep=' ').replace('&amp;', '&')
                price_dif = item.css('div.item .price_dif ::text').extract_first()
                price_dif = re.sub('.*?\[.*?([0-9.,]+).*?\].*', '\g<1>', price_dif).replace(',', '') if price_dif else None

                d = {
                    'item_id': item_id,
                    'name': name,
                    'qty': qty,
                    'price_dif': price_dif,
                }

                if subsection not in parts[section]:
                    parts[section][subsection] = []

                parts[section][subsection].append(d)

    return parts


def _parse_part_specs(response):
    d = {}

    section = None
    for tr in response.css('table.spec_tab_full_item tr'):
        if tr.css('th[colspan="2"]'):
            section = tr.css('th[colspan="2"] ::text').extract_first().strip()
            if section not in d:
                d[section] = {}
        elif tr.css('tr > td:nth-child(1)') and tr.css('tr > td:nth-child(2)'):
            key = _clear_and_join(tr.css('tr > td:nth-child(1)::text').extract(), sep=' ')
            value = _clear_and_join(tr.css('tr > td:nth-child(2)::text').extract())

            if section:
                d[section][key] = value
            else:
                d[key] = value

    return d
