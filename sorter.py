import json

input_file = 'broadberry.jl'
servers_output_file = 'broadberry_servers.jl'
parts_output_file = 'broadberry_parts.jl'

with open(input_file, 'r', encoding='utf-8') as fin:
    servers = open(servers_output_file, 'w', encoding='utf-8')
    parts = open(parts_output_file, 'w', encoding='utf-8')

    for line in fin:
        if '"type": "server"' in line:
            servers.write(line)
        elif '"type": "part"' in line:
            parts.write(line)
        else:
            print(line)

    servers.close()
    parts.close()
